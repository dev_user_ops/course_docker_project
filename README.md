# Time Application

В рамках данного курса было отработано умение написания docker-compose.yml файлов и организация взаимодействия backend, frontend, БД MySQL, сервиса Adminer. Исходные коды веб приложения взять из репозитория https://github.com/bstashchuk


Time application consists of the frontend and backend parts
Frontend is written with help of the Vue.js framework
Backend is written using Node.js and Express
Database is MySQL
